from django.shortcuts import render, get_object_or_404, redirect

from todos.models import TodoList, TodoItem
from .forms import TodoListForm


def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todos = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todos,
    }
    return render(request, "todos/detail.html", context)


def create_view(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)


def edit_view(request, id):
    view = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=view)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=view)
    context = {"todo_list_object": view, "form": form}
    return render(request, "todos/edit.html", context)


# def delete_list(request, id):
#     todolist = get_object_or_404(TodoList, id=id)
#     if request.method == "POST":
#         form = TodoListForm(request.POST, instance=todolist)
#         if form.is_valid():
#             todolist.delete()
#             return redirect("todo_list_list")
#     else:
#         form = TodoListForm(instance=todolist)
#     context = {
#         "form": form,
#     }
#     return render(request, "todos/delete.html", context)


def delete_todo_list(request, id):
    todo = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo)
        if form.is_valid():
            todo.delete()
            return redirect("todo_list_list")
    else:
        form = TodoListForm(instance=todo)
    context = {"form": form}

    return render(request, "todos/delete.html", context)
