from django.urls import path
from .views import (
    todo_list,
    todo_list_detail,
    create_view,
    edit_view,
    delete_todo_list,
)

urlpatterns = [
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("", todo_list, name="todo_list_list"),
    path("create/", create_view, name="create_view"),
    path("<int:id>/edit/", edit_view, name="edit_view"),
    path("<int:id>/delete/", delete_todo_list, name="todo_list_delete"),
]
