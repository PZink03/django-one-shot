from django.contrib import admin
from todos.models import TodoList, TodoItem


@admin.register(TodoList)
class TodolistAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )


@admin.register(TodoItem)
class TodoItemAdmim(admin.ModelAdmin):
    list_display = (
        "task",
        "due_date",
        "is_completed",
    )
